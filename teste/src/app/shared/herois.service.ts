import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Todos } from '../core/models/herois.model';

@Injectable({
  providedIn: 'root'
})
export class HeroisService {

  constructor(private http: HttpClient) { }

  public getherois():Observable<any>{
    return this.http.get(environment.apiHerois)
  }

}
