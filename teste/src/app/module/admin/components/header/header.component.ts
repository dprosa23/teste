import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  btnMobile = document.getElementById('btn-mobile');
  logo: string = '/assets/img/logo-coob+.png';
  login: string = '/assets/img/login.png';

  constructor() {}

  ngOnInit(): void {}

  toggleMenu() {
    const nav = document.getElementById('nav');
    nav?.classList.toggle('active');
  }
  fechar() {
    const nav = document.getElementById('nav');
    nav?.classList.toggle('active');
  }
}
