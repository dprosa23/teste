import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeusHeroisComponent } from './meus-herois.component';

describe('MeusHeroisComponent', () => {
  let component: MeusHeroisComponent;
  let fixture: ComponentFixture<MeusHeroisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MeusHeroisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MeusHeroisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
