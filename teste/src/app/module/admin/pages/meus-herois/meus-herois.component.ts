import { Component, OnInit } from '@angular/core';
import { Todos } from 'src/app/core/models/herois.model';
import { HeroisService } from 'src/app/shared/herois.service';

@Component({
  selector: 'app-meus-herois',
  templateUrl: './meus-herois.component.html',
  styleUrls: ['./meus-herois.component.scss'],
})
export class MeusHeroisComponent implements OnInit {
  heroisList: any[] = [];
  erro: any;
  imagem = 'assets/img/menu.png';
  p: number = 1;

  searchName: string = '';
  filterMetadata = { count: 0, isNestedSearch: true };

  constructor(private herois: HeroisService) {
    this.getterherois();
  }

  ngOnInit(): void {}

  getterherois() {
    this.herois.getherois().subscribe(
      (data: Todos[]) => {
        this.heroisList = data;
      },
      (error: any) => {
        this.erro = error;
        console.log('ERROR:', error);
      }
    );
  }
}
