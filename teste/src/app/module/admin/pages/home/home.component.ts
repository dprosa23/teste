import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  image = 'assets/img/menu.png';

  constructor(private toastr: ToastrService) {}

  ngOnInit(): void {}

  showToaster() {
    this.toastr.success('Sucesso ao Logar!');
  }
}
