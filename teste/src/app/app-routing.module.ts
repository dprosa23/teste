import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './core/components/login/login.component';
import { NotFoundComponent } from './core/components/not-found/not-found.component';
import { HomeComponent } from './module/admin/pages/home/home.component';
import { MeusHeroisComponent } from './module/admin/pages/meus-herois/meus-herois.component';

const routes: Routes = [

  {
    path:'', redirectTo: '/login', pathMatch: 'full'
  },
  {
    path:'login',component: LoginComponent
  },
  {
    path:'home',component: HomeComponent
  },
  {
    path:'meus-herois',component: MeusHeroisComponent
  },

  {
    path: '**', component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
